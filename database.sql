-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `budget`;
CREATE TABLE `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('pending','published','discarded') NOT NULL DEFAULT 'pending',
  `removed` tinyint(1) DEFAULT '0',
  `subcategory` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_category` (`id_category`),
  KEY `FK_budget_user` (`id_user`),
  CONSTRAINT `FK_budget_category` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_budget_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `category` (`id`, `name`) VALUES
(1,	'Calefacción'),
(2,	'Reformas Cocinas'),
(3,	'Reformas Baños'),
(4,	'Aire Acondicionado'),
(5,	'Construcción Casas');

DROP TABLE IF EXISTS `category_description`;
CREATE TABLE `category_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Índice 2` (`id_category`),
  CONSTRAINT `fk_category` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `category_description` (`id`, `text`, `id_category`) VALUES
(1,	'Presupuesto de colocación de 1 termo a gas-butano de 11 litros junker.',	1),
(2,	'Instalacion de caldera tres cuartos, hol pasillo y comedor de calefacion.',	1),
(3,	'Calefaccion en casa, necesitaria radiadores y calefactor ya que todo lo tengo a luz.',	1),
(4,	'Instalar termo de gas natural de 14L, sustituyendo el averiado. Con certificación.',	1),
(5,	'Buenos dias. Me gustaria instalar calefacción electrica en casa. Estoy interesada en poner radiadores electricos o suelo radiante. Entiendo que la segunda opción es mas cara y más complicada de instalar. Dispongo de suelo de tarima. La casa tiene aprox 53 m cuadrados. \n\nEn caso de radiadores electricos, serían instalar dos en el salon comedor, uno en la habitación grande, uno pequeño en la habitación pequeña y un radiador toallero en el baño.',	1),
(6,	'Instalación de caldera (en cocina) para producción de acs y que le de servicio también a una instalación de radiadores para salón y tres dormitorios. (instalación completa)\n\nSalón 22. 40\n\nDormitorios 7. 05, , 10. 70 y 5. 85\n\nAltura 2. 45.',	1),
(7,	'Comprar e instalalion de estufa de pallets para vivienda pareticular.',	1),
(8,	'Sustituir caldera existente por una caldera bazo nox. \n\nContactar via mail. \n\nGracias.',	1),
(9,	'Cambio de caldera de gas natural para calefacción y agua caliente.',	1),
(10,	'Calefaccion gas natural suelo radiante y ACS para un edificio unifamiliar de 4 alturas y sobre 100 metros por planta. La intención es instalar una sola caldera para todo el efificio pero estamos abiertos a escuchar otras opciones.',	1),
(11,	'Reformar cocina, cambiando azulejos, suelo, puntos de electricidad y fontanería y techo. Retirada de  muebles antiguos  aproximadamente a partir del mes de junio.',	2),
(12,	'Cocina para restaurante especializado en hamburguesas. Necesitaremos plancha, parrilla eléctrica, horno, freidoras, mesas de trabajo, mise en place, tostadoras, batidoras. Cámara frigorífica positivo y negativo.',	2),
(13,	'Instalar campana decorativa de 70 modelo tema dh2 70 inox, desmontando una similar que se encuentra actualmente instalada.',	2),
(14,	'Necesitaria marmolistas que toquen silestone para una cocina.',	2),
(15,	'Cambiar la encimera e cocina por una de piedra y simi, metros lineales: cinco en esquina.',	2),
(16,	'Necesito una cocina, solo muebles d abajo, la pared mide 3 metros.',	2),
(17,	'Cocina  rinconera de 2. 20x1. 70, estilo rustico.',	2),
(18,	'Renovar cocina. 3m x 2m aprox. \n\nTirar una pared.',	2),
(19,	'Necesito presupuesto para el cambio de una encimera en una cocina sin estrenar de material de formica a silestone o similar. Facilitaré las medidas a aquellos que se pongan en contacto conmigo. Gracias.',	2),
(20,	'Encimera aglomerado de color negro. \n\nMedidas 1, 80 x 0, 61 de fondo.',	2),
(21,	'Una mampara de ducha de una puerta abatible de 70 no me importaria que fuera barata de expocicion.',	3),
(22,	'Cambiar banera 150 x 70 por otra de igual tamaño ya comprada y en casa.',	3),
(23,	'Necesito arreglar el baño de casa, tengo que cambiar el plato de ducha (que es muy pequeño) y poner uno nuevo un poco más grande con mamparas. Lo más económico posible.',	3),
(24,	'Presupuesto para reformar un baño, en el que hay que quitar la bañera y poner un plato de ducha\n\n',	3),
(25,	'Necesito que me esmalten una bañera de hierro con patas, que la saneen y la pinten. En definitiva, que la restauren.',	3),
(26,	'Cambiar columna de hidromasaje por ducha.',	3),
(27,	'Reforma de baño alicatado y fontaneria.\n\nHay que quitarle el azulejo y los más importante es cambiar toda la fontanería ya que la distribución cambia; en un hueco nuevo hay que montar una ducha, y quitar de sitio los sanitarios ',	3),
(28,	'Reformar del baño, alicatado, cambiar bañera, cambiar lavabo, quitar racholas, .',	3),
(29,	'Cambiar  bañera por un pie de ducha  de lado a lado o sea grande, y colocar una  mampara de acrilico, y colocar alguna baldosa blanca hasta cubrir el espacio descubierto que deja la bañera.',	3),
(30,	'Cambiar baño completo. Azulejo, suelo, sanitarios, bañera por ducha. . . \n\nTiene aproximadamente 4. 7 metros cuadrados de superficie (276x186) y 2. 25 de alto\n\nNo me importa dejar el trabajo para el verano salvo que haya fuga. De omento se me han levantado las baldosas del suelo y no parece que haya agua.',	3),
(31,	'Presupuesto de mantenimiento i reparación de aire acondicionado de la marca  mitsubishi electric.',	4),
(32,	'Sustituir una bomba de calor roof top ventilador axial averiado de potencias frio/calor del orden de 10 kCal/h y kF/h, caudal : 2. 400 m3/h.',	4),
(33,	'Instalacion aire acondicionado/bomba de calor para un salon de actos de unos 100 m2. Calidad alta/media.',	4),
(34,	'Instsalar aire acondicionat en un menjador de 35 m2.',	4),
(35,	'Instalar aire condicionado en el salon y en el dormitorio \n\nSalon mide 28, 50 m* \n\nDormitorio mide 16 m*.',	4),
(36,	'Tengo un equipo de aire acondicionado/calefaccion que tiene una perdida de gas. Necesito que detecten la fuga y recargar de nuevo de gas.',	4),
(37,	'Aire acondicionado mitsubishi electric en valencia, \n\nEl suministro y instalacion del equipo. Solo en el salon la innstalacion. \n\nPrecio economico. En naquera, valencia.',	4),
(38,	'Hola, tengo una casa de unos 50 m2 con sobrepiso abierto, q me aconsejáis? por lo q he leido la bomba de calor podria encajarme. . . Espero vuestro consejo e información, gracias!!',	4),
(39,	'Aire acondicionado por conductos, para 4 dormitorios.',	4),
(40,	'Instalación de aire acondicionado es un compresor y tres splits, dos son para dos habitaciones y el otro para el salón, pero están en plantas diferentes, las habitaciones están contiguas y el salón está en la planta inferior.',	4),
(41,	'Necesito presupuesto para construir una casa de dos plantas en churriana de la vega (Granada). Tengo la parcela de 220 metros cuadrados en propiedad. Se trataría de una casa de 3 plantas con 5 o más habitaciones. Estoy pendiente de adquirir el proyecto.',	5),
(42,	'Construir una casa desde cero. \n\nTengo localizado un terreno en arroyo de la miel en una zona urbanizada y  tiene los servicios básicos. \n\nEl terreno está nivelado y tiene unos 450m2\n\nLa casa tendría 4 habitaciones 3  baños lubina  cocina y comedor. Aproximadamente 150 m2.',	5),
(43,	'Presupuesto para cubrir terraza de 6mts de ancho por 3´70mts de largo, con inclinación de unos 30º, los materiales necesarios se detallan a continuación, agradeceria se detalle el presupuesto. \n\n6 bigas de 3. 70x16x8. \n\n1 biga de 6. 00x16x16. \n\n22m2 de termochip con acabado machimbrado de pino. \n\n22m2 de honduline. \n\n22m2 de teja md. Arabe. \n\n2 ventanas mavlux practicables con acabado de pino 118x65 c/u. \n\nFrontal de aluminio con tres ventanas abatibles acabado blanco 50x110 c/u aproximadamente. \n\nMateriales y mano de obra. \n\nTotal.',	5),
(44,	'Quisiera saber precio de construir una casa para ir a hablar con mi banco y llevar una cantidad aproximada.',	5),
(45,	'Chalet de 4 habitaciones con baño amplio salon amplio porche de 160 m2 +porche de 30m2+ piscina+ garage 2 plazas + perimetro.',	5),
(46,	'Necesito de profesionales en la construcción de 6 unifamiliares en madrid de 709 m2 construidos. Estructura metálica, cerramiento en tosco o termo arcilla, mono capa. Interior en pladur, tarima flotante en las dos plantas y gres en cocina, baños y aseo.',	5),
(47,	'Demoler una casa vieja de 100 m2 y construir una nueva en planta de 150m2 de 4 o 5 habitaciones con garaje debajo de la casa y una habitación al lado del garaje.\n\nSólo he visto la casa, y quiera saber si es viable este proyecto.',	5),
(48,	'200 metros lineales de vigas de hierro HEB 250 y 200 metros lineales de IPN 250.',	5),
(49,	'Hola javi, soy eddy, he perdido el  móvil y toda la agenda, me podrías llamar para tener tu número? muchas gracias.',	5),
(50,	'Presupuesto ampliar casa\n\nTerreno desnivelado\n\nSant vicents dels horts. Barcelona\n\nReformar planta y anadir un piso.',	5);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `direction` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2020-05-17 19:27:16
