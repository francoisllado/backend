# Habitissimo Backend Challenge

##Technologies used and reasons why I use them
- PHP 5.6.4 (It was the version of php I had installed on my pc at the time I started programming, due to the complexity of the project I did not see the need to use a more current version)
- MySQL 5.7.24
- EzSQL (This is a class to facilitate access to the database. I'm used to using it)
- Slim (Micro framework used only to create the API)
- Composer for PHP dependencies
## Installation

First you have to create a database with the attached SQL file in the root (database.sql)
Once the database is created, you must access the /config.php file and indicate where our database is located
the database username, password, database and server
```
$db_user = "root";
$db_pass ="";
$db_name = "challenge";
$db_server = "127.0.0.1";
```


## Project structure

```
api/ -> Directory where the API code is located.
api/src -> Directory where the API source code, paths and their settings are located.
src/core -> Directory where the classes I use to call the database are located
vendor/ -> Directory where the dependencies installed by composer are
config.php
composer.json
composer.lock
```
