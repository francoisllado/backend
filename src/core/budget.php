<?php

class Budget extends BaseCore{
	function get($id){
		if(empty($id)){
			return false;
		}
		$query = "select * from budget where id = $id";
		return $this->db->get_row($query);
	}

	function getAll($email){
		$query = "select b.* from budget as b 
					left join user as u on b.id_user = u.id
					where removed = 0 ";
		if(!empty($email)){
			$query .= " and u.email = '$email'";
		}
		return $this->db->get_results($query);
	}

	function create($info){

		if(!empty($info)){
			if($this->core->user->create($info['user'])){
				$user                      = $this->core->user->getByEmail($info['user']['email']);
				$info['budget']['id_user'] = $user->id;
				$info['budget']['status']  = 'pending';
				$query                     = $this->queryInsert('budget', $info['budget']);
				return $this->db->query($query);
			}
		}
		return false;
	}

	function checkStatus($id){
		$query = "select * from budget where id = $id and status = 'pending' and removed = 0";
		return $this->db->get_row($query);
	}

	function update($info){
		$query = $this->queryUpdate('budget', $info);
		return $this->db->query($query);
	}

	function publish($id){
		$current_budget = $this->get($id);
		$info           = ['id' => $id, 'status' => 'published'];
		if(!empty($current_budget->title) && !empty($current_budget->id_category) && $current_budget->status == 'pending'){
			return $this->update($info);
		}
		return false;
	}

	function discard($id){
		$current_budget = $this->get($id);
		$info           = ['id' => $id, 'status' => 'discarded'];
		if($current_budget->status != 'discarded'){
			return $this->update($info);
		}
		return false;
	}
}