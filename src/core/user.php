<?php

class User extends BaseCore{
	function getByEmail($email){
		$query = "select * from `user` where email = '$email'";
		return $this->db->get_row($query);
	}

	function create($info){
		if(empty($info['email'])){return false;}
		$check_email = $this->getByEmail($info['email']);
		if(empty($check_email)){
			$query = $this->queryInsert('user', $info);
			return $this->db->query($query);
		}else{
			$info['id'] = $check_email->id;
			$query      = $this->queryUpdate('user', $info);
			$this->db->query($query);
			return true;
		}
	}

	function update($info){
		$query = $this->queryUpdate('user', $info);
		return $this->db->query($query);
	}
}