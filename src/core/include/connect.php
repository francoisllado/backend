<?php
if(!class_exists("ezSQL_mysql")) {
	require_once("ez_sql/shared/ez_sql_core.php");
	require_once("ez_sql/mysql/ez_sql_mysql.php");
}
$db = new ezSQL_mysql($db_user,$db_pass,$db_name,$db_server);
$db->query("SET NAMES utf8");