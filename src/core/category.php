<?php

class Category extends BaseCore{
	function get($id){
		$query = "select * from category where id = $id";
		return $this->db->get_row($query);
	}

	function getByName($name){
		$query = "select * from category where name = '$name'";
		return $this->db->get_row($query);
	}

	function getAllMatchSuggestion(){
		$query = "select * from category_description";
		return $this->db->get_results($query);
	}

	function getSuggestions($description_text){
		$map_suggestions   = $this->getAllMatchSuggestion();
		$array_suggestions = [];
		foreach($map_suggestions as $suggestion){
			if(self::checkText($description_text, $suggestion->text)){
				$array_suggestions[] = $suggestion->id_category;
			}
		}
		$simplified_array    = array_unique($array_suggestions);
		$suggestions_to_show = [];
		foreach($simplified_array as $id_category){
			$category_info         = (array) $this->get($id_category);
			$suggestions_to_show[] = $category_info;
		}
		return $suggestions_to_show;
	}

	//Function for checking if a text matches another
	static function checkText($searchPat, $sourceStr){

		$arraySource  = explode(" ", $sourceStr);
		$arrayPattern = explode(" ", $searchPat);
		$sourceStr = addslashes($sourceStr);
		$searchPat = addslashes($searchPat);
		$result    = array_intersect($arraySource, $arrayPattern);
		//First we check if the source and search strings are similar or the same
		if(preg_match("~\b$searchPat\b~", $sourceStr) || $searchPat == $sourceStr){
			return true;
			//In case we dont match anything, we try to search word by word
		}elseif(!empty($result)){
			return true;
		}else{
			//If everything fails, it means that there is no description that matches us and there is no category to suggest
			return false;
		}
	}

	function getSuggestionsBudget($id_budget){
		$info_budget = $this->core->budget->get($id_budget);
		if(empty($info_budget)){
			return false;
		}
		return $this->getSuggestions($info_budget->description);
	}

	function updateDescriptionsFromCSV($excel_route){
		$category_descriptions = $fields = array();
		$i                     = 0;
		$handle                = @fopen($excel_route, "r");
		if($handle){
			while(($row = fgetcsv($handle, 4096)) !== false){
				if(empty($fields)){
					$fields = $row;
					continue;
				}
				foreach($row as $k => $value){
					$category_descriptions[$i][$fields[$k]] = $value;
				}
				$i++;
			}
			if(!feof($handle)){
				echo "Error: unexpected fgets() fail\n";
			}
			fclose($handle);
		}
		$array_count  = count($category_descriptions);
		$insert_count = 0;
		foreach($category_descriptions as $categoryDescription){
			$category = $this->getByName($categoryDescription['Categoría']);
			$info     = ['id_category' => $category->id, 'text' => $categoryDescription['Descripción']];
			$query    = $this->queryInsert('category_description', $info);
			if($this->db->query($query)){
				$insert_count++;
			}
		}
		$result = "$insert_count entries were inserted into the database from the $array_count CSV entries";
		return $result;
	}
}