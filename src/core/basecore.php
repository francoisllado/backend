<?php
//CLASE PRINCIPAL DEL SISTEMA, DE ESTA DESCIENDEN TODAS LAS DEM�S, HEREDANDO AS� SUS ATRIBUTOS (DB,CORE) Y SUS FUNCIONES
//EL METODO PARA USAR LOS ATRIBUTOS (DB,CORE) Y LAS FUNCIONES (QUERY...) ES: $this->"atributo/funcion" DENTRO DE LA CLASE
abstract class BaseCore{
	protected $db;
	protected $core;

	//constructor
	function __construct($db, $core){
		$this->db   = $db;
		$this->core = $core;
	}

	//función que genera y devuelve la query insert a raiz del nombre de la tabla y un array ($object)
	//Los índices del array ($object) tienen que coincidir con los campos de la tabla en la que hacemos el insert
	protected function queryInsert($table, $object){
		if(!empty($object)){
			$keys   = "(".implode(",", array_keys($object)).")";
			$values = "(";
			foreach($object as $value){
				if($value == 'null'){
					$values .= "null,";
				}elseif(is_numeric($value)){
					$values .= "".addslashes($value).",";
				}else{
					$values .= "'".addslashes($value)."',";
				}
			}
			$values = rtrim($values, ",");
			$values .= ")";
			$query  = "insert into $table $keys VALUES $values;";
			return $query;
		}
		return false;
	}

	//función que genera y devuelve la query update a raiz de la tabla y un array ($object)
	//Los índices del array ($object) tienen que coincidir con los campos de la tabla en la que hacemos el insert
	//uno de ellos tiene que ser OBLIGATORIO el "id"
	protected function queryUpdate($table, $object){

		$id = $object["id"];
		unset($object["id"]);
		$query = "update $table set";
		foreach($object as $key => $value){
			if(is_numeric($value)){
				$query .= " $key=".addslashes($value).",";
			}elseif(strtolower($value) == 'null'){
				$query .= " $key=".addslashes($value).",";
			}else{
				$query .= " $key='".addslashes($value)."',";
			}
		}
		$query = rtrim($query, ",");
		if(is_numeric($id)){
			$id_where = $id;
		}else{
			$id_where = "'$id'";
		}
		$query .= " where id = $id_where;";
		return $query;
	}

	protected function queryDelete($table, $id){
		$query = "update $table set removed=1,active=0 where id = '$id'";
		return $query;
	}
}