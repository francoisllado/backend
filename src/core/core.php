<?php
//connect db require
require_once($coreRootPath.'/include/connect.php');

//funcion que carga una clase (SOLO SI ESTA EN EL MISMO DIRECTORIO) de forma automatica cuando se intenta crear una instancia de esta
spl_autoload_register(function ($nombre_clase) {
	//variable $class_directory = define el directorio en el que se encuentran las clases
	global $class_directory;
	if(file_exists(__DIR__."/".strtolower($nombre_clase) . '.php')){
		require_once(__DIR__."/".strtolower($nombre_clase) . '.php');
		//        throw new Exception("<strong>ERROR: </strong> Imposible cargar $nombre_clase.");
	}

});

class Core{
	function __construct($db){
		$this->db = $db;
	}

	public function __get($service_name){
		$core = $this;
		$service_class=ucfirst($service_name);

		try{
			$core->$service_name = new $service_class($this->db, $core);
		} catch (Exception $e) {
			echo $e->getMessage(), "\n";
			exit();
		}

		return $this->$service_name;
	}
}

$core = new Core($db);