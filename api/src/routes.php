<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Path to get a user, you have to pass it an email
$app->get('/user/', function(Request $request, Response $response){
	$user_mail = $request->getParam('email');
	$info_user = $this->core->user->getByEmail($user_mail);
	try{
		if(!empty($info_user)){
			$result['result'] = $info_user;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on save booking process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'Required request params not found';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to create/update a user, you have to pass it a json with the user info
$app->post('/user/create/', function(Request $request, Response $response){
	$new_user    = $request->getParsedBody();
	$info_result = $this->core->user->create($new_user);
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on save user process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'No update or creation for the user';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to create a budget, you have to pass it json with the budget info
$app->post('/budget/create/', function(Request $request, Response $response){
	$new_budget  = $request->getParsedBody();
	$info_result = $this->core->budget->create($new_budget);
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on save user process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'Lack of information to be able to create the new budget';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to update a budget, you have to pass it json with the budget info
$app->put('/budget/update/', function(Request $request, Response $response){
	$budget_info  = $request->getParsedBody();
	$check_status = $this->core->budget->checkStatus($budget_info['id']);
	$info_result  = '';
	if($check_status){
		$info_result = $this->core->budget->update($budget_info);
	}
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on update budget process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'The budget is not on pending status or does not exist, you cant edit it';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to publish a budget, you have to pass it a budget ID
$app->put('/budget/publish/', function(Request $request, Response $response){
	$budget_id   = $request->getParsedBody();
	$info_result = $this->core->budget->publish($budget_id['id']);
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on update budget process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'The budget has not title, category or its status is not pending or already published';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to publish a discard, you have to pass it a budget ID
$app->put('/budget/discard/', function(Request $request, Response $response){
	$budget_id   = $request->getParsedBody();
	$info_result = $this->core->budget->discard($budget_id['id']);
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on update budget process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'The budget  is  already discarded';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to get a budget list a budget, you have to pass a user email if you want to see all the user's budgets
$app->get('/budget/list/', function(Request $request, Response $response){
	$budget_email = $request->getParam('email');
	$info_result  = $this->core->budget->getAll($budget_email);
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on update budget process.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'No budgets related to this email';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});
//Path to get a budget category suggesion, you have to pass a budget ID
$app->get('/category/suggestions/', function(Request $request, Response $response){
	$budget_id   = $request->getParam('id');
	$info_result = $this->core->category->getSuggestionsBudget($budget_id);
	try{
		if(!empty($info_result)){
			$result['result'] = $info_result;
			if($result['result'] == null){
				$result['error']        = true;
				$result['errorMessage'] = 'Null result on getting suggestions for this budget.';
			}
		}else{
			$result['error']        = true;
			$result['errorMessage'] = 'No categories suggested for this budget';
		}
	}catch(Exception $e){
		$result['error']        = true;
		$result['errorMessage'] = 'ServerException: '.$e->getMessage();
	}
	$response->getBody()->write(json_encode($result));
	return $response;
});


