<?php
return [
	'settings' => [
		'determineRouteBeforeAppMiddleware' => false,
		'displayErrorDetails' => true, // set to false in production
		'addContentLengthHeader' => false, // Allow the web server to send the content-length header
	],
	'core' => $core,
];